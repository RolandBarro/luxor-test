// import Image from 'next/image'

import { useEffect, useState } from "react";
import { Pokemon } from "../../models/pokemon.model";

interface PaginationProps {
  pages: number;
  // paginationClicks: (elem: string) => void;
}

interface BtnLabels {
  prev: string;
  next: string;
}
 
export const Pagination: React.FC<PaginationProps> = (props) => {
  const { pages } = props;
  const [pageCount, setPageCount] = useState<number[]>([1]);
  const [ btnLabels, setBtnLabel ] = useState<BtnLabels>({
    prev: 'Prev',
    next: 'Next',
  });
  
  // const handleClick = (elem: string) => {
  //   paginationClicks(elem);
  // };

  const createBtnItems = pageCount.map(btn => {
    return (
      <button type="button" className="page-btn" key={btn}>{ btn }</button>
    );
  });

  useEffect(() => {
    let pageCnt: number[] = [];

    for (let i = 0; i < pages; i++) {
      pageCnt.push(i + 1);
    }

    setPageCount(pageCnt);

    if (window.innerWidth < 425) {
      setBtnLabel({ prev: '<', next: '>' });
    }
  }, [pages]);

  return (
    <div className="pagination-container">
      <div className="page-btn-list">
        { createBtnItems }
      </div>
      <div className="page-navigate-container">
        <button className="page-btn">{ btnLabels.prev }</button>
        <button className="page-btn">{ btnLabels.next }</button>
      </div>
    </div>
  );
}
 