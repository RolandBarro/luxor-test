// import Image from 'next/image'

import { useEffect, useState } from "react";
import { Pokemon } from "../../models/pokemon.model";

export interface DeplayCounterProps {
  value: string;
}
 
export const DeplayCounter: React.FC<DeplayCounterProps> = (props) => {
  const [ counter, updateCounter ] = useState<number>(0);
  const [ unit, updateUnit ] = useState<string>();
  const { value } = props;
  
  useEffect(() => {
    const numbers = /^([0-9]+(\.[0-9]+)?)/gm;

    let um = value.replace(numbers, '');
    updateUnit(um);

    let count = parseFloat(value) * 100;
    
    let itr = 0;
    
    let interval = setInterval(() => { 
      if (itr >= count) clearInterval(interval);
      updateCounter(itr / 100);
      itr++;
    });
  }, [value])


  return (
    <span>{ counter }{unit}</span>
  );
}
 