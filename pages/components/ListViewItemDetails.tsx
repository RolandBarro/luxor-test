// import Image from 'next/image'

import { useEffect, useState } from "react";
import { Pokemon } from "../../models/pokemon.model";
import { DeplayCounter } from "./DelayCounter";

interface ListViewItemDetailsProps {
  pokemon: Pokemon;
}

interface ViewLabels {
  min: string;
  max: string;
}
 
export const ListViewItemDetails: React.FC<ListViewItemDetailsProps> = (props) => {
  const { pokemon } = props;
  const [ viewLabels, setViewLabels ] = useState<ViewLabels>({
    min: 'Minimum',
    max: 'Maximum',
  });

  useEffect(() => {
    if (window.innerWidth < 425) {
      setViewLabels({
        min: 'Min',
        max: 'Max',
      });
    }
  }, []);

  return (
    <div className="listViewItemDetails">
      <div className="header">
        <div className="pokemon-name">{ pokemon.name }</div>
        <div className="pokemon-number">#{ pokemon.number }</div>
      </div>
      <div className="detail-container">
        <div className="image-container">
          <img src={pokemon.image} alt="image" />
        </div>
        <div className="details">
          <div className="card">
            <div className="card-header">Height</div>
            <div className="card-body">
              <div className="detail-min"> 
                <div>{ viewLabels.min }: </div>
                <div><DeplayCounter value={pokemon.height.minimum} /></div>
              </div>
              <div className="detail-max">
                <div>{ viewLabels.max }: </div>
                <div><DeplayCounter value={ pokemon.height.maximum } /></div>
                </div>
            </div>
          </div>

          <div className="card">
            <div className="card-header">Weight</div>
            <div className="card-body">
              <div className="detail-min"> 
              <div>{ viewLabels.min }: </div>
                <div><DeplayCounter value={ pokemon.weight.minimum } /></div>
              </div>
              <div className="detail-max">
                <div>{ viewLabels.max }: </div>
                <div><DeplayCounter value={ pokemon.weight.maximum } /></div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
 