// import Image from 'next/image'

import { Pokemon } from "../../models/pokemon.model";

export interface ListViewItemProps {
  pokemon: Pokemon;
  onItemClick: (pokemon: Pokemon) => void;
  selected: (pokemon: Pokemon) => boolean;
}
 
export const ListViewItem: React.FC<ListViewItemProps> = (props) => {
  const { pokemon, onItemClick, selected } = props;
  const handleClick = () => {
    onItemClick(pokemon);
  };

  return (
    <div className={'listViewItem ' + (selected(pokemon) ? 'is-selected' : '' )} onClick={handleClick}>
      <div className="img-container">
        <img src={pokemon.image} alt="image" />
      </div>
      <div className="pokemon-number" >{ pokemon.number }</div>
      <div className="pokemon-name">{ pokemon.name }</div>
    </div>
  );
}
 