import React, { useEffect, useState } from "react";
import { Pokemon } from "../models/pokemon.model";
import { API } from "./api/APIService";
import { ListViewItem } from "./components/ListViewItem";
import { ListViewItemDetails } from "./components/ListViewItemDetails";
import { Pagination } from "./components/Pagination";

const ListView: React.FC = () => {
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [selectedPokemon, setSelectedPokemon] = useState<Pokemon>();

  const fetchData = () => {
    const url = `https://graphql-pokemon2.vercel.app/`;
    const query = `{
      pokemons(first: 10) {
        id
        number
        name
        image
        classification
        weight {
          minimum
          maximum
        }
        height {
          minimum
          maximum
        }
      }
    }`;
    API({ url, query })
      .then((response) => {
        console.log(response.data);
        const { pokemons } = response.data;
        setPokemons(pokemons);
        setSelectedPokemon(pokemons[0]);
        console.log(pokemons[0]);
      });
  };

  const handleItemClick = (pokemon: Pokemon) => {
    setSelectedPokemon(pokemon);
  }

  const paginationClicks = (elem: string) => {
    console.log({ elem });
  }

  const isSelected = (pokemon: Pokemon) => {
    return selectedPokemon ? selectedPokemon.id === pokemon.id : false;
  }

  const pokemonItem = pokemons.map((pokemon: Pokemon) => {
    return <ListViewItem pokemon={pokemon} key={pokemon.number} selected={ isSelected } onItemClick={handleItemClick} /> 
  });
  
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="listViewContainer dark:bg-gray-800 w-screen h-screen flex place-content-center">
      <div className="listViewContentContainer flex flex-wrap content-center">
        <div className="sidebarListContainer">
          <div className="sidebarList">
            { pokemons && pokemons.length ? pokemonItem  : 'Loading...' }
          </div>
          <div className="pagination">
            <Pagination pages={4} />
          </div>
        </div>
        <div className="detailView">
          { selectedPokemon ? <ListViewItemDetails pokemon={selectedPokemon} />  : 'Loading...' }
        </div>  
      </div>
    </div>
  );
}
export default ListView;
