import type { NextPage } from 'next'
import { useState } from 'react';
import { useRouter } from 'next/router'

interface LoginData {
  userName: string;
  password: string;
}

const Home: NextPage = () => {
  const router = useRouter();
  const [ userName, setUserName ] = useState<string>('');
  const [ password, setPassword ] = useState<string>('');

  const formData = {
    username: {
      value: userName,
      isValid: false
    },
    password: {
      value: password,
      isValid: false
    }
  }

  let formIsValid = false;
  let isSubmitting = false;

  const handleSubmit = (e: any) => {
    e.preventDefault();
    console.log('onSubmit: ', e);
    router.push('/list-view');
  }

  const onChange = (event: any) => {
    event.preventDefault();

    const { name, value } = event.target;
    switch(name) {
      case 'userName':
        setUserName(value);
        console.log('formData: ', formData);
        break;
      case 'password':
        setPassword(value);
        console.log('formData: ', formData);
        break;
    }
  };

  return (
    <div className="dark:bg-gray-800 w-screen h-screen flex place-content-center">
      <div className="flex flex-wrap content-center">
        <div className="loginContainer">
          <form className="login-form" name="loginForm" onSubmit={handleSubmit} method="post">
            <div className="formGroup">
              <input className="formControl" type="text" name="userName" placeholder="User Email"
                required
                value={formData.username.value} 
                onChange={onChange} />
              {
                !userName ?
                <p className="text-red-500 text-xs italic">Username is required.</p>
                : ''
              }
            </div>

            <div className="formGroup">
              <input className="formControl" type="password" name="password" placeholder="Password"
                required
                value={formData.password.value}
                onChange={onChange} />
              {
                !password ?
                <p className="text-red-500 text-xs italic">Password is required.</p>
                : ''
              }
            </div>

            {/* TODO: implemented simple form validation - <required> */}
            <button type="submit" className="submitBtn" disabled={ !userName || !password }>
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Home
