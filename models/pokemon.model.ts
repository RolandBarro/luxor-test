interface MinMax {
  minimum: string;
  maximum: string;
}
export interface Pokemon {
  id: number | string;
  name: string;
  number: number | string;
  image: string;
  height: MinMax;
  weight: MinMax;
}
